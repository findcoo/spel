package spel;

public class Types {
  private int count;
  private String message;
  private double frequency;
  private double capacity; 
  private String name;
  private Boolean enabled;
  private Instrument instrument;

  public Types () {

  }
  
  public void display() {
    System.out.println(this.toString());
  }
  
  /**
   * @param count the count to set
   */
  public void setCount(int count) {
    this.count = count;
  }

  /**
   * @param message the message to set
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * @param frequency the frequency to set
   */
  public void setFrequency(double frequency) {
    this.frequency = frequency;
  }


  /**
  * @param capacity the capacity to set
  */
  public void setCapacity(double capacity) {
    this.capacity = capacity;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @param enabled the enabled to set
   */
  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  /**
   * @param instrument the instrument to set
   */
  public void setInstrument(Instrument instrument) {
    this.instrument = instrument;
  }
}
