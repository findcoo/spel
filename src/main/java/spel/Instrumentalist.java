package spel;

public class Instrumentalist implements Performer {
  private Instrument instrument;
  private String song;

  public Instrumentalist () {

  }

  @Override
  public void perform() {
    // TODO Auto-generated method stub
    System.out.println(this.song);

  }

  /**
   * @param instrument the instrument to set
   */
  public void setInstrument(Instrument instrument) {
    this.instrument = instrument;
  }

  /**
  * @return the song
  */
  public String getSong() {
    return song;
  }

  /**
   * @param song the song to set
   */
  public void setSong(String song) {
    this.song = song;
  }

}
