package spel;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main{

  public static void main(String args[]) {

   ApplicationContext atx = new ClassPathXmlApplicationContext("spel.xml");
   Types types= (Types) atx.getBean("type");
   types.display();

   Performer performer = (Performer) atx.getBean("carl");
   performer.perform();

  }
}

