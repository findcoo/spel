##SpEL 기초

1. *디렉토리 구성*
```

./src/
`-- main
    |-- java
    |   `-- spel
    |       |-- Instrument.java
    |       |-- Main.java
    |       |-- Performer.java
    |       |-- Saxophone.java
    |       `-- Types.java
    `-- resources
        `-- spel.xml

```
---
2. *주의 및 참고*
    + `/src/main/resources/spel.xml` 참조
    + `"#{value}"` 표현식이다. 변수형을 자동으로 잡아주기 때문에 간편하다.(리터럴 값)
    + `<p:instrument value="#{saxophone}">` ref대신 표현식을 통해 빈 참조가 가능하다.
    + `<property name="song" value="#{kenny.song}"/>` 빈의 프로퍼티 명으로 get, set 메소드를 참조할 수 있다.
    + 이외에도 클래스의 메소드명으로 접근하여 메소드 반환 값을 참조할 수 있다.
---


